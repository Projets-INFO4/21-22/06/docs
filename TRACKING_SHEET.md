# YEAR 2021-2022

## Week 8: 28/03

- Fix last bugs
- APK creation
- Report and slides writing

## Week 7: 21/03

- Change navigation system with state variable
- Changed all the strings
  - Strings are now variables
  - Strings are translated in french and english
- Continue function retrieval
  - Add function selection view and add copy to clipboard

## Week 6: 14/03

- Refactoring done
  - Change the logo
- Fix a bug with the camera scanner
  - No longer stays active after scanning
- Continue function retrieval
  - The correct function is found

## Week 5: 28/02

- Interview with the teacher in charge
- Finish Helium API
- Continue encoding function retrieval
  - Add the ability to select the manufacturer, the device, the version...
- Begin the refactoring of the workflow and the design

## Week 4: 13/02

- Continue implementation of Helium API
  - Finish API calls to the API
  - Add the ability to fill in the fields necessary
- Begin automation of encoding functions retrieval
  - Analyse of the GitHub repository containing functions

## Week 3: 6/02

- Begin implementation of Helium API in the application
  - Make calls to the API
  - Add the ability to enter the API key on the app

## Week 2: 31/01

- Subject comprehension
  - Our main goal is to add the Helium API to the app
  - Maybe later we could improve the user workflow of the app
- Begin researches about Helium API
  - Test the Console by adding devices manually
  - Read the documentation to find the usefull informations for our application
- Try to make the already existing project work
  - Try to build it in debug mode
  - Try to install it on our phones
- Discuss about the new workflow we might implement later

## Week 1: 24/01

- Choice of the project subject
  - We are all interested in developing a mobile application
  - Some of us already have some experience in mobile development
- Team discovery
  - We never worked together on a project
  - We share our views and expectations on the project
