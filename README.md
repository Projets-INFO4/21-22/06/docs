# PROJECT 6 : MOBILE APPLICATION REGISTERING IoT NODES

The LoRaSCAN application, available on Android and iOS, reads LoRaWAN QR codes and automatically registers them on LoRaWAN network operators, following the *TR005 LoRaWAN® Device Identification QR Codes specification*.

## Networks

The following networks are currently available :

- The Things Network (TTN)
- Campus IoT
- Helium

## References

- [TR005 LoRaWAN® Device Identification QR Codes](https://lora-alliance.org/resource_hub/tr005-lorawan-device-identification-qr-codes/)
- [LoRa Alliance® Vendor ID for QR Code](https://lora-alliance.org/resource_hub/lora-alliance-vendor-id-for-qr-code/)

## Tools installation

- Install Visual Studio Code (or any other IDE)
- Follow the [environnement setup](https://reactnative.dev/docs/environment-setup) for React Native
- Clone this repository

## Project setup

- Open a terminal at the root of the repository
- Run ```npm install --global yarn``` to install `yarn`
- Run ```yarn install``` to install dependencies

## Installation

- Enable debug mode on your smartphone and connect it to your computer if you want to debug on your smartphone
- Run ```npx react-native run-android``` to start the Metro server
- Run ```npx react-native start``` to launch the app (on the emulator and on your smartphone if it is connected)
